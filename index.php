<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S1 Activity</title>
</head>
<body>

	<h1>Full Address</h1>
	<p><?= getFullAddress('Philippines', 'Quezon City', 'Metro Manila', '3F Caswynn Bldg., Timog Avenue') ;?></p>
	<p><?= getFullAddress('Philippines', 'Makati City', 'Metro Manila', '3F Enzo Bldg., Buendia Avenue') ;?></p>

	<h2>Letter-Based Grading</h2>
	<p>87 <?=getLetterGrade(87); ?></p>	
	<p>94 <?=getLetterGrade(94); ?></p>	
	<p>74 <?=getLetterGrade(74); ?></p>	

</body>
</html>